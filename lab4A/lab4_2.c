//#####################################################################
//	Имя файла:	_lab5А_.c
//
//	Описание:	Выработка синусоидальных колебаний на выводе T1PWM
// 		(GPIO A6). Выработка прерываний CPU - таймером 0
// 		каждые 50 мс. Сброс сторожевого таймера каждые 0,2 с.
//#####################################################################

#include "DSP281x_Device.h"
#include "IQmathLib.h"				// Включение математических библиотек
#pragma DATA_SECTION(sine_table,"IQmathTables");
_iq30 sine_table[512];

void Gpio_select(void);
void InitSystem(void);
interrupt void T1_Compare_isr(void); // Подпрограмма обработки прерываний
// таймера сравнения 1 EVA
int Count = 0;

void main(void)
{
    InitSystem();		// Инициализация регистров ЦСП
    Gpio_select();		// Инициализация линий ввода/вывода
    InitPieCtrl();		// Инициализации блока PIE
    InitPieVectTable(); // Инициализация таблицы векторов блока PIE

    EALLOW;
    PieVectTable.T1CINT = &T1_Compare_isr;
    EDIS;

    // Разрешение прерывания от таймера сравнения 1, группа 2, прерывание 5
    PieCtrlRegs.PIEIER2.bit.INTx5 = 1;

    IER = 2;			// Разрешение прерываний линии 2 (INT2)
    EINT;   			// Общее разрешение прерываний INTM
    ERTM;   			// Разрешение общих прерываний в режиме
    // реального времени

    // Настройка модуля Менеджера Событий A

    EvaRegs.GPTCONA.bit.TCMPOE = 0; 	// Управление логикой сравнения
    // T1PWM / T2PWM
    // Полярность выходов сравнения GP таймера 1: активный уровень - низкий
    EvaRegs.GPTCONA.bit.T1PIN = 1;

    EvaRegs.T1CON.bit.FREE = 1;		// Биты управления эмулятором:
    EvaRegs.T1CON.bit.SOFT = 1;		// мгновенная остановка
    EvaRegs.T1CON.bit.TMODE = 1;	// Непрерывный счет вверх
    EvaRegs.T1CON.bit.TPS = 1;		// Предделитель = 1 : 75 МГц
    EvaRegs.T1CON.bit.TENABLE = 1;	// Запрещение работы GP таймера 1
    EvaRegs.T1CON.bit.TCLKS10 = 1;	// Внутренний источник тактирования
    EvaRegs.T1CON.bit.TCLD10 = 1;	// Перезагрузка GP таймера
    // при достижении нуля
    EvaRegs.T1CON.bit.TECMPR = 1;	// Разрешение режима сравнения

    EvaRegs.T1PR = 1500;			// Регистр периода GP таймера

    EvaRegs.T1CMPR = EvaRegs.T1PR/2;

    EvaRegs.EVAIMRA.bit.T1CINT = 1;	// Разрешение прерывания при
    // достижении значения сравнения
    EvaRegs.T1CON.bit.TENABLE = 1;	// Разрешение работы GP таймера 1

    while(1)
    {
        EALLOW;
        SysCtrlRegs.WDKEY = 0xAA;
        EDIS;
    }
}

//#####################################################################
//	Подпрограмма:	T1_Compare_isr
//
//	Описание:	 Обработка прерывания по GP таймеру 1
//#####################################################################

interrupt void T1_Compare_isr(void)
{
    static int index=0;

    Count++;

    EALLOW;
    SysCtrlRegs.WDKEY = 0x55;
    EDIS;

    EvaRegs.T1CMPR =  EvaRegs.T1PR -_IQsat(_IQ30mpy(sine_table[index]+_IQ30(0.9999),EvaRegs.T1PR/2),EvaRegs.T1PR,0);

    index +=4;          		// Использование каждого 4-го значения таблицы
    if (index >511) index = 0;

    // Сброс флага прерывания GP таймера сравнения
    EvaRegs.EVAIFRA.bit.T1CINT = 1;

    // Подтверждение прерывания от группы прерываний 2
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;
}

//#####################################################################
//	Подпрограмма:	Gpio_select
//
//	Описание:	Настройка линий порта GPIO B15-8 на ввод, а линий B7-0 //		на вывод. Настройка всех линий портов A, D, F, E, G на
//		ввод. Запрещение работы входного ограничителя
//#####################################################################

void Gpio_select(void)
{
    EALLOW;
    GpioMuxRegs.GPAMUX.all = T1PWM;	// Настройка линий ввода/вывода на
    // Настройка линии порта GPIO A6 на работу в качестве специальной
    // функции (T1PWM) - вывод прямоугольных импульсов

    GpioMuxRegs.GPBMUX.all = 0x0;	// работу в качестве портов
    GpioMuxRegs.GPDMUX.all = 0x0;
    GpioMuxRegs.GPFMUX.all = 0x0;
    GpioMuxRegs.GPEMUX.all = 0x0;
    GpioMuxRegs.GPGMUX.all = 0x0;

    GpioMuxRegs.GPADIR.all = 0x0;		// Настройка портов А, D, E, F, G на ввод
    GpioMuxRegs.GPBDIR.all = 0x00FF;	// Настройка линий 15-8 на ввод, а линий
    GpioMuxRegs.GPDDIR.all = 0x0;		// 7-0 на вывод
    GpioMuxRegs.GPEDIR.all = 0x0;
    GpioMuxRegs.GPFDIR.all = 0x0;
    GpioMuxRegs.GPGDIR.all = 0x0;

    GpioMuxRegs.GPAQUAL.all = 0x0;	// Запрещение входного ограничителя
    GpioMuxRegs.GPBQUAL.all = 0x0;
    GpioMuxRegs.GPDQUAL.all = 0x0;
    GpioMuxRegs.GPEQUAL.all = 0x0;
    EDIS;
}

//#####################################################################
//	Подпрограмма:	InitSystem
//
//	Описание:	Настройка сторожевого таймера на работу,
//		предделитель = 1. Выработка сброса сторожевым
//		таймером. Внутренняя частота работы ЦСП 150 МГц.
//		Запись в предделитель высокоскоростного таймера
//		коэффициента деления 2, а в предделитель
//		низкоскоростного таймера - 4. Запрещение работы
//		периферийных устройств
//#####################################################################

void InitSystem(void)
{
    EALLOW;
    SysCtrlRegs.WDCR = 0x00AF;		// Разрешение работы сторожевого   								// таймера, предделитель = 64
    // 0x00E8 - запрещение работы  сторо-
    // жевого таймера, предделитель = 1

    SysCtrlRegs.SCSR = 0; 			// Выработка сброса WDT
    SysCtrlRegs.PLLCR.bit.DIV = 10;	// Настройка блока умножения частоты
    SysCtrlRegs.HISPCP.all = 0x1; 	// Задание значения предделителя высо-
    SysCtrlRegs.LOSPCP.all = 0x2; 	// коскоростного и низкоскоростного
    // таймеров

    SysCtrlRegs.PCLKCR.bit.EVAENCLK=0;	// Запрещение работы периферий-
    SysCtrlRegs.PCLKCR.bit.EVBENCLK=0;	// ных устройств
    SysCtrlRegs.PCLKCR.bit.SCIAENCLK=0;
    SysCtrlRegs.PCLKCR.bit.SCIBENCLK=0;
    SysCtrlRegs.PCLKCR.bit.MCBSPENCLK=0;
    SysCtrlRegs.PCLKCR.bit.SPIENCLK=0;
    SysCtrlRegs.PCLKCR.bit.ECANENCLK=0;
    SysCtrlRegs.PCLKCR.bit.ADCENCLK=0;
    EDIS;
}

