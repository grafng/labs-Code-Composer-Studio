// FILE:	Lab9.c
// TITLE:DSP28 CAN Transmission
//CPU Timer0 ISR every 50 ms
//Watchdog active , served in ISR and main-loop
//CAN message : 1 Byte ( status of GPIO B15-B8) every 1 second
//			Baud Rate 100KBPS
//			Identifier :  0x1000 0000
//			Mailbox #5
#include "DSP281x_Device.h"   // Включение заголовочного файла

void Gpio_select(void);
void SpeedUpRevA(void);
void InitSystem(void);
void InitCan();
interrupt void cpu_timer0_isr(void);
// Prototype for Timer 0 Interrupt Service Routine

void main(void)
{
struct ECAN_REGS ECanaShadow;

InitSystem();		// Инициализация регистров ЦСП

// Speed_up the silicon A Revision.
// No need to call this function for Rev. C  later silicon versions

Gpio_select();		// Инициализация линий ввода/вывода
InitPieCtrl();// Function Call to init PIE-unit ( code : DSP281x_PieCtrl.c)

InitPieVectTable(); // Function call to init PIE vector table ( code : DSP281x_PieVect.c )

// re-map PIE - entry for Timer 0 Interrupt
EALLOW;  // This is needed to write to EALLOW protected registers
   PieVectTable.TINT0 = &cpu_timer0_isr;
   EDIS;    // This is needed to disable write to EALLOW protected registers

InitCpuTimers();

// Configure CPU-Timer 0 to interrupt every 50 ms:
// 150MHz CPU Freq, 50000 µseconds interrupt period
    ConfigCpuTimer(&CpuTimer0, 150, 50000);

    // Enable TINT0 in the PIE: Group 1 interrupt 7
   PieCtrlRegs.PIEIER1.bit.INTx7 = 1;

// Enable CPU INT1 which is connected to CPU-Timer 0:
    IER = 1;

// Enable global Interrupts and higher priority real-time debug events:
   EINT;   // Enable Global interrupt INTM
   ERTM;   // Enable Global realtime interrupt DBGM

   InitCan();
     /* Write to the MSGID field  */
    ECanaMboxes.MBOX5.MSGID.all     = 0x????????;
  ECanaMboxes.MBOX5.MSGID.bit.IDE = ?;  // Установка идентификатора сообщения

 /* Установка Mailbox 5 передающим */
ECanaShadow.CANMD.all = ECanaRegs.CANMD.all;
ECanaShadow.CANMD.bit.MD5 = ?;
ECanaRegs.CANMD.all = ECanaShadow.CANMD.all;
/* Активация Mailbox 5 */
ECanaShadow.CANME.all = ECanaRegs.CANME.all;
ECanaShadow.CANME.bit.ME5 = ?;
ECanaRegs.CANME.all = ECanaShadow.CANME.all;

/* Установка длины сообщения равной 1 */
ECanaMboxes.MBOX5.MSGCTRL.bit.DLC = ?;
CpuTimer0Regs.TCR.bit.TSS = 0;
  while(1)
{
  while(CpuTimer0.InterruptCount < 20)
{			 // wait for Timer 0
    EALLOW;
    SysCtrlRegs.WDKEY = 0xAA;		// and serve watchdog #2
    EDIS;
}
CpuTimer0.InterruptCount = 0;		// reset number of timer interrupts
    ECanaMboxes.MBOX5.MDL.byte.BYTE0 = (GpioDataRegs.GPBDAT.all>>8 ) ;
     ECanaShadow.CANTRS.all = ?;
     ECanaShadow.CANTRS.bit.TRS5 = ?;     // Посыл запроса передачи Mailbox 5
     ECanaRegs.CANTRS.all = ECanaShadow.CANTRS.all;

     while(ECanaRegs.CANTA.bit.TA5 == 0 ) {}  // Когда бит TA5 установлен..

     ECanaShadow.CANTA.all = ?;
     ECanaShadow.CANTA.bit.TA5 = ?;     	 // Установка бита TA5 в исходное состояние
     ECanaRegs.CANTA.all = ECanaShadow.CANTA.all;
    }
}

void Gpio_select(void)
{
    EALLOW;
    GpioMuxRegs.GPAMUX.all = 0x?;	// Настройка линий ввода/вывода на работу в качестве портов
    GpioMuxRegs.GPBMUX.all = 0x?;
    GpioMuxRegs.GPDMUX.all = 0x?;
    GpioMuxRegs.GPFMUX.all = 0x?;
    GpioMuxRegs.GPFMUX.bit.CANTXA_GPIOF6 = ?;    // Настройка периферийных функций CANTXA и
    GpioMuxRegs.GPFMUX.bit.CANRXA_GPIOF7 = ?;	 //	CANRXA для работы с CAN-модулем
    GpioMuxRegs.GPEMUX.all = 0x?;
    GpioMuxRegs.GPGMUX.all = 0x?;
    GpioMuxRegs.GPADIR.all = 0x?;	// Настройка портов А, D, E, F, G на ввод
    GpioMuxRegs.GPBDIR.all = 0x????;  // Настройка линий 15-8 на ввод, а линий 7-0 на вывод
    GpioMuxRegs.GPDDIR.all = 0x?;
    GpioMuxRegs.GPEDIR.all = 0x?;
    GpioMuxRegs.GPFDIR.all = 0x?;
    GpioMuxRegs.GPGDIR.all = 0x?;

    GpioMuxRegs.GPAQUAL.all = 0x?;	// Запрещение входного ограничителя
    GpioMuxRegs.GPBQUAL.all = 0x?;
    GpioMuxRegs.GPDQUAL.all = 0x?;
    GpioMuxRegs.GPEQUAL.all = 0x?;
    EDIS;
}

void InitSystem(void)
{
   EALLOW;
    SysCtrlRegs.WDCR= 0x00AF;		// Работа сторожевого таймера
     // 0x00E8  запрещение работы  сторожевого таймера, предделитель = 1
     // 0x00AF  Разрешение работы сторожевого таймера, предделитель = 64
   SysCtrlRegs.SCSR = 0; 			// Выработка сброса WDT
   SysCtrlRegs.PLLCR.bit.DIV = 10;	// Настройка блока умножения частоты
   SysCtrlRegs.HISPCP.all = 0x1; // Задание значения предделителя высокоскоростного таймера
  SysCtrlRegs.LOSPCP.all = 0x2; // Задание значения предделителя низкоскоростного таймера
        // Запрещение работы периферийных устройств кроме eCAN
    SysCtrlRegs.PCLKCR.bit.EVAENCLK=0;
    SysCtrlRegs.PCLKCR.bit.EVBENCLK=0;
    SysCtrlRegs.PCLKCR.bit.SCIAENCLK=0;
    SysCtrlRegs.PCLKCR.bit.SCIBENCLK=0;
    SysCtrlRegs.PCLKCR.bit.MCBSPENCLK=0;
    SysCtrlRegs.PCLKCR.bit.SPIENCLK=0;
    SysCtrlRegs.PCLKCR.bit.ECANENCLK=1;
    SysCtrlRegs.PCLKCR.bit.ADCENCLK=0;
    EDIS;
}
interrupt void cpu_timer0_isr(void)
{
      CpuTimer0.InterruptCount++;   // Serve the watchdog every Timer 0 interrupt
     EALLOW;
    SysCtrlRegs.WDKEY = 0x55;		// Serve watchdog #1
   EDIS;
   // Acknowledge this interrupt to receive more interrupts from group 1
   PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
void InitCan(void)
{
   asm("  EALLOW");
/* Configure eCAN RX and TX pins for eCAN transmissions using eCAN regs*/
    ECanaRegs.CANTIOC.bit.TXFUNC = ?;
    ECanaRegs.CANRIOC.bit.RXFUNC = ?;
/* Configure eCAN for HECC mode - (reqd to access mailboxes 16 thru 31) */
// HECC mode also enables time-stamping feature
ECanaRegs.CANMC.bit.SCB = ?;
/* Configure bit timing parameters */
ECanaRegs.CANMC.bit.CCR = ? ;            // Установка CCR для получения доступа к регистрам времени
  while(ECanaRegs.CANES.bit.CCE != 1 ) {}   // Когда ССЕ (флаг для передачи запроса инициализации CAN)установлен..
    ECanaRegs.CANBTC.bit.BRPREG = ??;     // Установка параметров BRP,TSEG1 и TSEG2
    ECanaRegs.CANBTC.bit.TSEG2REG = ?;    // таким образом, чтобы скорость передачи
    ECanaRegs.CANBTC.bit.TSEG1REG = ??;   // была 100кбит/с

    ECanaRegs.CANMC.bit.CCR = ? ;             // Сброс CCR регистра CANMC для запрета доступа к регистру CANBTC
    while(ECanaRegs.CANES.bit.CCE == !0 ) {}   // Когда CCE сброшен..
}
/* Disable all Mailboxes  */
//=================================================
// End of SourceCode.
//=================================================

