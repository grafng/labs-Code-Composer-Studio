// FILE:	Lab10.c
// TITLE:	DSP28 CAN Receive ,  Mailbox 1
//Identifier 0x10 000 000 ; Baudrate 100 KBPS
//Data Byte 1 of CAN - Frame will be copied to 8 LED's (GPIOB7 - B0)
//Watchdog active , served in main-loop

#include "DSP281x_Device.h"  // Включение заголовочного файла

void Gpio_select(void);
void InitSystem(void);
void InitCan(void);

void main(void)

{
struct ECAN_REGS ECanaShadow;

InitSystem();		// Инициализация регистров ЦСП
Gpio_select();		// Инициализация линий ввода/вывода
InitCan();

/* Write to the MSGID field - MBX number is written as its MSGID */
   ECanaMboxes.MBOX1.MSGID.all  = 0x????????;  // Установка идентификатора сообщения
   ECanaMboxes.MBOX1.MSGID.bit.IDE = ?;

/* Установка Mailbox 1 принимающим */
ECanaShadow.CANMD.all = ECanaRegs.CANMD.all;
ECanaShadow.CANMD.bit.MD1 = ?;
ECanaRegs.CANMD.all = ECanaShadow.CANMD.all;

/* Активация Mailbox 1 */
ECanaShadow.CANME.all = ECanaRegs.CANME.all;
ECanaShadow.CANME.bit.ME1 = ?;
ECanaRegs.CANME.all = ECanaShadow.CANME.all;
while(1)
{
    do
    {
     ECanaShadow.CANRMP.all = ECanaRegs.CANRMP.all;
EALLOW;
    SysCtrlRegs.WDKEY = 0x55;       //  serve watchdog #1
    SysCtrlRegs.WDKEY = 0xAA;		//  serve watchdog #2
EDIS;
    }
  while(ECanaShadow.CANRMP.bit.RMP1 != 1 );       // Когда RMP1 установлен..

     GpioDataRegs.GPBDAT.all = ECanaMboxes.MBOX1.MDL.byte.BYTE0;
 ECanaShadow.CANRMP.bit.RMP1 = ?;
ECanaRegs.CANRMP.all = ECanaShadow.CANRMP.all;
// Clear RMP1 bit and start again
 }
}

void Gpio_select(void)
{
EALLOW;
GpioMuxRegs.GPAMUX.all = 0x?;	// Настройка линий ввода/вывода на работу в качестве портов
    GpioMuxRegs.GPBMUX.all = 0x?;
    GpioMuxRegs.GPDMUX.all = 0x?;
    GpioMuxRegs.GPFMUX.all = 0x?;
    GpioMuxRegs.GPEMUX.all = 0x?;
    GpioMuxRegs.GPGMUX.all = 0x?;
    GpioMuxRegs.GPFMUX.bit.CANTXA_GPIOF6 = ?;  // Настройка периферийных функций CANTхA и CANRхA
    GpioMuxRegs.GPFMUX.bit.CANRXA_GPIOF7 = ?;

    GpioMuxRegs.GPADIR.all = 0x?;	// Настройка портов А, D, E, F, G на ввод
    GpioMuxRegs.GPBDIR.all = 0x????;	// Настройка линий 15-8 на ввод, а линий 7-0 на вывод
    GpioMuxRegs.GPDDIR.all = 0x?;
    GpioMuxRegs.GPEDIR.all = 0x?;
    GpioMuxRegs.GPFDIR.all = 0x?;
    GpioMuxRegs.GPGDIR.all = 0x?;

    GpioMuxRegs.GPAQUAL.all = 0x?;  // Запрещение входного ограничителя
    GpioMuxRegs.GPBQUAL.all = 0x?;
    GpioMuxRegs.GPDQUAL.all = 0x?;
    GpioMuxRegs.GPEQUAL.all = 0x?;
    EDIS;
}

void InitSystem(void)
{
   EALLOW;
   SysCtrlRegs.WDCR= 0x00AF;		// Работа сторожевого таймера
    // 0x00E8  запрещение работы  сторожевого таймера, предделитель = 1
    // 0x00AF  Разрешение работы сторожевого таймера, предделитель = 64

   SysCtrlRegs.SCSR = 0; 			// Выработка сброса WDT
   SysCtrlRegs.PLLCR.bit.DIV = 10;	// Настройка блока умножения частоты
   SysCtrlRegs.HISPCP.all = 0x1; // Задание значения предделителя высокоскоростного таймера
  SysCtrlRegs.LOSPCP.all = 0x2;  // Задание значения предделителя низкоскоростного таймера

   // Запрещение работы периферийных устройств кроме еСAN
    SysCtrlRegs.PCLKCR.bit.EVAENCLK=0;
    SysCtrlRegs.PCLKCR.bit.EVBENCLK=0;
    SysCtrlRegs.PCLKCR.bit.SCIAENCLK=0;
    SysCtrlRegs.PCLKCR.bit.SCIBENCLK=0;
    SysCtrlRegs.PCLKCR.bit.MCBSPENCLK=0;
    SysCtrlRegs.PCLKCR.bit.SPIENCLK=0;
    SysCtrlRegs.PCLKCR.bit.ECANENCLK=1;
   SysCtrlRegs.PCLKCR.bit.ADCENCLK=0;
   EDIS;
}

void InitCan(void)
{
asm("  EALLOW");
/* Configure eCAN RX and TX pins for eCAN transmissions using eCAN regs*/

    ECanaRegs.CANTIOC.bit.TXFUNC = ?;
    ECanaRegs.CANRIOC.bit.RXFUNC = ?;

/* Configure eCAN for HECC mode - (reqd to access mailboxes 16 thru 31) */
// HECC mode also enables time-stamping feature
ECanaRegs.CANMC.bit.SCB = ?;

/* Configure bit timing parameters */
ECanaRegs.CANMC.bit.CCR = ? ;         // Установка CCR для получения доступа к регистрам времени
    while(ECanaRegs.CANES.bit.CCE != 1 ) {}   // Когда CCE (флаг для передачи запроса инициализации CAN) установлен..
    ECanaRegs.CANBTC.bit.BRPREG = ??;
    ECanaRegs.CANBTC.bit.TSEG2REG = ?;
    ECanaRegs.CANBTC.bit.TSEG1REG = ??;

    ECanaRegs.CANMC.bit.CCR = ? ;             // Сброс бита CCR регистра CANMС для запрета доступа к регистру CANBTC
    while(ECanaRegs.CANES.bit.CCE == !0 ) {}   // Когда CCE сброшен..

/* Disable all Mailboxes  */

 ECanaRegs.CANME.all = ?;   // Отключить все Mailboxеs кроме того который отправляет сообщение
asm("  EDIS");
}
//===========================================================
// End of SourceCode.
//===========================================================

