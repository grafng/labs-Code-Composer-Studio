//#####################################################################
//	Имя файла:	lab4_.c
//
//	Описание:	Выработка прямоугольных импульсов. Воспроизведение
// 		гаммы через динамик, подключенный к выводу T1PWM
// 		(GPIO A6). Длительность звучания каждой ноты 0,5 с.
// 		Выработка прерываний CPU - таймером 0 каждые 50 мс.
//		Сброс сторожевого таймера каждые 0,2 с.
//#####################################################################

#include "DSP281x_Device.h"

void Gpio_select(void);
void InitSystem(void);
interrupt void cpu_timer0_isr(void);  	// Подпрограмма обработки прерываний от
                                        // CPU - таймера 0

void main(void)
{
    unsigned int i;
    unsigned long time_stamp; 	// Временная фиксация
    int frequency[8]={2219,1973,1776,1665,1480,1332,1184,1110};

    InitSystem();		// Инициализация регистров ЦСП
    Gpio_select();		// Инициализация линий ввода/вывода
    InitPieCtrl();		// Вызов подпрограммы инициализации блока PIE
    InitPieVectTable(); // Вызов подпрограммы инициализации таблицы
                        // векторов блока PIE

    EALLOW;
    PieVectTable.TINT0 = &cpu_timer0_isr;
    EDIS;

    InitCpuTimers();

    // Настройка CPU - таймера 0: частота 150 МГц, период прерывания 50 мс
    ConfigCpuTimer(&CpuTimer0, 150, 50000);

    // Разрешение прерывания от CPU - таймера 0, группа 1, прерывание 7
    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;

    IER = 1;			// Разрешение прерываний линии 1 (INT1)
    EINT;   			// Общее разрешение прерываний INTM
    ERTM;   			// Разрешение общих прерываний в режиме
                        // реального времени

    EALLOW;
    // Настройка модуля Менеджера Событий A
    EvaRegs.GPTCONA.bit.TCMPOE = 1; 	// Управление логикой сравнения
                                        // T1PWM / T2PWM
    // Полярность выходов сравнения таймера 1: активный уровень - низкий
    EvaRegs.GPTCONA.bit.T1PIN = 1;
    // Режим счета вверх таймера-счетчика модуля Менеджера Событий
    EvaRegs.T1CON.all = 1;

    CpuTimer0Regs.TCR.bit.TSS = 0;	// Запуск CPU - таймера 0
    EDIS;

    i = 0;
    time_stamp = 0;

    while(1)
    {
        if ((CpuTimer0.InterruptCount%4)==0)// Деление значения счетчика
                                            // на 4 без остатка
           {
            EALLOW;
            SysCtrlRegs.WDKEY = 0xAA;
            EDIS;
           }
        if ((CpuTimer0.InterruptCount - time_stamp)>10)
        {
            time_stamp = CpuTimer0.InterruptCount;
            if(i<7) EvaRegs.T1PR = frequency[i++];
            else  	EvaRegs.T1PR = frequency[14-i++];

            EvaRegs.T1CMPR = EvaRegs.T1PR/2;
            EvaRegs.T1CON.bit.TENABLE = 1; 	// Разрешение работы
                                            // таймера модуля EVA
            if (i>=14) i=0;
        }
    }
}

//#####################################################################
//	Подпрограмма:	Gpio_select
//
//	Описание:	Настройка линий порта GPIO B15-8 на ввод, а линий B7-0 //		на вывод. Настройка всех линий портов A, D, F, E, G на
//		ввод. Запрещение работы входного ограничителя
//#####################################################################

void Gpio_select(void)
{
    EALLOW;
    GpioMuxRegs.GPAMUX.all = T1PWM;		// Настройка линий ввода/вывода на
                                        // Настройа линии порта GPIO A6 на работу в качестве специальной
                                        // функции (T1PWM) - вывод прямоугольных импульсов
    GpioMuxRegs.GPBMUX.all = 0x0;		// работу в качестве портов
    GpioMuxRegs.GPDMUX.all = 0x0;
    GpioMuxRegs.GPFMUX.all = 0x0;
    GpioMuxRegs.GPEMUX.all = 0x0;
    GpioMuxRegs.GPGMUX.all = 0x0;

    GpioMuxRegs.GPADIR.all = 0x0;		// Настройка портов А, D, E, F, G на ввод
    GpioMuxRegs.GPBDIR.all = 0x00FF;	// Настройка линий порта B 15-8 на ввод,
    GpioMuxRegs.GPDDIR.all = 0x0;		// а линий 7-0 на вывод
    GpioMuxRegs.GPEDIR.all = 0x0;
    GpioMuxRegs.GPFDIR.all = 0x0;
    GpioMuxRegs.GPGDIR.all = 0x0;

    GpioMuxRegs.GPAQUAL.all = 0x0;		// Запрещение входного ограничителя
    GpioMuxRegs.GPBQUAL.all = 0x0;
    GpioMuxRegs.GPDQUAL.all = 0x0;
    GpioMuxRegs.GPEQUAL.all = 0x0;
    EDIS;
}

//#####################################################################
//	Подпрограмма:	InitSystem
//
//	Описание:	Настройка сторожевого таймера на работу,
//		предделитель = 1. Выработка сброса сторожевым
//		таймером. Внутренняя частота работы ЦСП 150 МГц.
//		Запись в предделитель высокоскоростного таймера
//		коэффициента деления 2, а в предделитель
//		низкоскоростного таймера - 4. Запрещение работы
//		периферийных устройств
//#####################################################################

void InitSystem(void)
{
    EALLOW;
    CpuTimer0Regs.TCR.bit.TSS = 0;
    SysCtrlRegs.WDCR= 0x00AF;		// Разрешение работы сторожевого   								// таймера, предделитель = 64
                                    // 0x00E8 - запрещение работы  сторо-
                                    // жевого таймера, предделитель = 1

    SysCtrlRegs.SCSR = 0; 			// Выработка сброса WDT
    SysCtrlRegs.PLLCR.bit.DIV = 10;	// Настройка блока умножения частоты
    SysCtrlRegs.HISPCP.all = 0x1; 	// Задание значения предделителя высо-
    SysCtrlRegs.LOSPCP.all = 0x2; 	// коскоростного и низкоскоростного
                                    // таймеров

    // Разрешение / запрещение тактирования периферийных устройств
    SysCtrlRegs.PCLKCR.bit.EVAENCLK = 1;
    SysCtrlRegs.PCLKCR.bit.EVBENCLK = 0;
    SysCtrlRegs.PCLKCR.bit.SCIAENCLK = 0;
    SysCtrlRegs.PCLKCR.bit.SCIBENCLK = 0;
    SysCtrlRegs.PCLKCR.bit.MCBSPENCLK = 0;
    SysCtrlRegs.PCLKCR.bit.SPIENCLK = 0;
    SysCtrlRegs.PCLKCR.bit.ECANENCLK = 0;
    SysCtrlRegs.PCLKCR.bit.ADCENCLK = 0;

    EDIS;
}

//#####################################################################
//	Подпрограмма:	cpu_timer0_isr
//
//	Описание:	 Обработка прерывания по CPU - таймеру 0
//#####################################################################

interrupt void cpu_timer0_isr(void)
{
    CpuTimer0.InterruptCount++;

    EALLOW;
    SysCtrlRegs.WDKEY = 0x55;		// Сброс сторожевого таймера
    EDIS;

    // Подтверждение прерывания от группы прерываний 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

