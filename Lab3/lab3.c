//##################################################################
//	��� �����:	Lab3.c
//
//	��������:	� ������� 8 �����������, ������������ � ������ 
//		����� GPIOB0 - GPIOB7, ����������� "������� ����".
//		����������� �������� ������ - ������ � ��������
//#####################################################################

#include "DSP281x_Device.h" // ��������� ������������� �����

void delay_loop(long);
void Gpio_select(void);
void InitSystem(void);

interrupt void cpu_timer0_isr(void);

void main(void)
{
	unsigned int i;
	unsigned int LED[8]= {0x0001,0x0002,0x0004,0x0008,
	                      0x0010,0x0020,0x0040,0x0080};	
	
	InitSystem();		// ������������� ��������� ���
	Gpio_select();		// ������������� ����� �����/������ 
	
	InitPieCtrl();
	InitPieVectTable();

	EALLOW;
	PieVectTable.TINT0 = &cpu_timer0_isr;
	EDIS;

		
	InitCpuTimers();
	ConfigCpuTimer(&CpuTimer0, 150, 50000);		

	PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
	IER = 1;
	EINT; 
	ERTM;

	CpuTimer0Regs.TCR.bit.TSS = 0;


while(1)
	{    
  	    for(i=0;i<14;i++)
  		{
    	     if(i<7) 	GpioDataRegs.GPBDAT.all = LED[i];
    	     else  	GpioDataRegs.GPBDAT.all = LED[14-i]; 
    	     delay_loop(?);
    	 }
     }
} 	



//#####################################################################
//	������������:	delay_loop
//
//	��������:	������������ ��������� �������� ������� �����������
//#####################################################################

interrupt void cpu_timer0_isr(void)
{
	a'sdflk'asd;lfka'sd;lf
}


void delay_loop(long end)
{
	long i;
	for (i = 0; i < end; i++);
	EALLOW; // ����� ����������� �������
	SysCtrlRegs.WDKEY = 0x?;
	SysCtrlRegs.WDKEY = 0x?;
	EDIS;
}




//#####################################################################
//	������������:	Gpio_select
//
//	��������:	��������� ����� ����� GPIO B15-8 �� ����, � ����� B7-0 
//		�� �����. ��������� ���� ����� ������ A, D, F, E, G �� 
//		����. ���������� ������ �������� ������������
//#####################################################################

void Gpio_select(void)
{
	EALLOW;
    GpioMuxRegs.GPAMUX.all = 0x?; // ��������� ����� �����/������ ��                           
    GpioMuxRegs.GPBMUX.all = 0x?; // ������ � �������� ������   
    GpioMuxRegs.GPDMUX.all = 0x?;
    GpioMuxRegs.GPFMUX.all = 0x?;		 
    GpioMuxRegs.GPEMUX.all = 0x?; 
    GpioMuxRegs.GPGMUX.all = 0x?;			
										
    GpioMuxRegs.GPADIR.all = 0x?; // ��������� ������ �, D, E, F, G �� ����
    GpioMuxRegs.GPBDIR.all = 0x?; // ��������� ����� 15-8 �� ����, 
    GpioMuxRegs.GPDDIR.all = 0x?; // � ����� 7-0 �� �����
    GpioMuxRegs.GPEDIR.all = 0x?;	
    GpioMuxRegs.GPFDIR.all = 0x?;	
    GpioMuxRegs.GPGDIR.all = 0x?;	

    GpioMuxRegs.GPAQUAL.all = 0x?; // ���������� �������� ������������
    GpioMuxRegs.GPBQUAL.all = 0x?;
    GpioMuxRegs.GPDQUAL.all = 0x?;
    GpioMuxRegs.GPEQUAL.all = 0x?;
    EDIS;
}     

//#####################################################################
//	������������:	InitSystem
//
//	��������:	��������� ����������� ������� �� ������,
//		������������ = 1. ��������� ������ ���������� 
//		��������. ���������� ������� ������ ��� 150 ���.
//		������ � ������������ ����������������� ������� 
//		������������ ������� 2, � � ������������
//		���������������� ������� - 4. ���������� ������ 
//		������������ ���������
//#####################################################################

void InitSystem(void)
{
   	EALLOW;
   	SysCtrlRegs.WDCR= 0x?;		// ���������� ������ �����������   							// �������, ������������ = 64
						// ��� ���������� ������  �����-
						// ������ �������, ������������ = 1
 
   	SysCtrlRegs.SCSR = ?; 		// ���������������� ������ ��� �� WDT	
   	SysCtrlRegs.PLLCR.bit.DIV = ?;// ��������� ����� ��������� �������
      SysCtrlRegs.HISPCP.all = 0x?;	// ������� �������� �����������������   	SysCtrlRegs.LOSPCP.all = 0x?; // � ���������������� ������������� 
     	

   	SysCtrlRegs.PCLKCR.bit.EVAENCLK=?;	// ���������� ������
   	SysCtrlRegs.PCLKCR.bit.EVBENCLK=?;	// ������������ ���������
   	SysCtrlRegs.PCLKCR.bit.SCIAENCLK=?;
   	SysCtrlRegs.PCLKCR.bit.SCIBENCLK=?;
   	SysCtrlRegs.PCLKCR.bit.MCBSPENCLK=?;
   	SysCtrlRegs.PCLKCR.bit.SPIENCLK=?;
   	SysCtrlRegs.PCLKCR.bit.ECANENCLK=?;
   	SysCtrlRegs.PCLKCR.bit.ADCENCLK=?;
   	EDIS;
}
