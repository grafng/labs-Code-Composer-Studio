//#####################################################################
//        ��� �����:        _Lab2_.c
//
//        ��������:        � ������� 8 �����������, ������������ � ������ 
//                ����� GPIOB0 - GPIOB7, ����������� "������� ����".
//                ����������� �������� ������ - ������ � ��������
//#####################################################################

#include "DSP281x_Device.h" // ��������� ������������� �����

void delay_loop(long);
void Gpio_select(void);
void InitSystem(void);

interrupt void cpu_timer0_isr(void); 

void main(void)
{
        unsigned int i;
        unsigned int LED[8]= {0x0001,0x0002,0x0004,0x0008,
                              0x0010,0x0020,0x0040,0x0080};        
        
        InitSystem();                // ������������� ��������� ���
        Gpio_select();                // ������������� ����� �����/������ 
		InitPieCtrl();
		IntPieVectTable();
		EALLOW;
		PieVectTable.TINT0=&cpu_timer0_isr;
		EDIS;
		InitCpuTimers();
		ConfigCpuTimer(&CpuTimer0,150,50000);
		PieCtrlRegs.PIEIER1.bit.INTx7=1;
		IER=1;
		EINT;
		ERTM;
		CpuTimer0Regs.TCR.bit.TSS=0;

        
while(1)
        {    
              for(i=0;i<14;i++)
                  {
                 if(i<7)         GpioDataRegs.GPBDAT.all = LED[i];
                 else          GpioDataRegs.GPBDAT.all = LED[14-i]; 
				 while(CpuTimer0.InterruptCount<3);
				 CpuTimer0.InterruptCount=0;
                 
             }
     }
}         

//#####################################################################
//        ������������:        delay_loop
//
//        ��������:        ������������ ��������� �������� ������� �����������
//#####################################################################

/*void delay_loop(long end)
{
        long i;
        for (i = 0; i < end; i++);
        EALLOW;                                                // ����� ����������� �������
        SysCtrlRegs.WDKEY = 0x55;
        SysCtrlRegs.WDKEY = 0xAA;
        EDIS;
}*/

//#####################################################################
//        ������������:        Gpio_select
//
//        ��������:        ��������� ����� ����� GPIO B15-8 �� ����, � ����� B7-0 
//                �� �����. ��������� ���� ����� ������ A, D, F, E, G �� 
//                ����. ���������� ������ �������� ������������
//#####################################################################

void Gpio_select(void)
{
        EALLOW;
        GpioMuxRegs.GPAMUX.all = 0;        // ��������� ����� �����/������ ��                           
    GpioMuxRegs.GPBMUX.all = 0;        // ������ � �������� ������   
    GpioMuxRegs.GPDMUX.all = 0;
    GpioMuxRegs.GPFMUX.all = 0;                 
    GpioMuxRegs.GPEMUX.all = 0; 
    GpioMuxRegs.GPGMUX.all = 0;                        
                                                                                
    GpioMuxRegs.GPADIR.all = 0xFF;                     // ��������� ������ �, D, E, F, G �� ����
    GpioMuxRegs.GPBDIR.all = 0xFF;       			   // ��������� ����� 15-8 �� ����, � ����� 
    GpioMuxRegs.GPDDIR.all = 0xFF;         		       // 7-0 �� �����
    GpioMuxRegs.GPEDIR.all = 0xFF;        
    GpioMuxRegs.GPFDIR.all = 0xFF;        
    GpioMuxRegs.GPGDIR.all = 0xFF;        

    GpioMuxRegs.GPAQUAL.all = 0x0;        // ���������� �������� ������������
    GpioMuxRegs.GPBQUAL.all = 0x0;
    GpioMuxRegs.GPDQUAL.all = 0x0;
    GpioMuxRegs.GPEQUAL.all = 0x0;
    EDIS;
}     

//#####################################################################
//        ������������:        InitSystem
//
//        ��������:        ��������� ����������� ������� �� ������,
//                ������������ = 1. ��������� ������ ���������� 
//                ��������. ���������� ������� ������ ��� 150 ���.
//                ������ � ������������ ����������������� ������� 
//                ������������ ������� 2, � � ������������
//                ���������������� ������� - 4. ���������� ������ 
//                ������������ ���������
//#####################################################################

void InitSystem(void)
{
           EALLOW;
           SysCtrlRegs.WDCR= 0x00E8;                // ���������� ������ �����������                                                                   
                                                                        // �������, ������������ = 64
                                                                        // ��� ���������� ������  �����-
                                                                        // ������ �������, ������������ = 1
 
           SysCtrlRegs.SCSR = 0;                        // ���������������� ������ ��� �� WDT        
           SysCtrlRegs.PLLCR.bit.DIV = 10;        // ��������� ����� ��������� �������
           SysCtrlRegs.HISPCP.all = 1;         // ������� �������� ������������ ����-
           SysCtrlRegs.LOSPCP.all = 2;         // ������������� � ���������������� 
                                                                        // ��������             

           SysCtrlRegs.PCLKCR.bit.EVAENCLK=0;        // ���������� ������ ���������-
           SysCtrlRegs.PCLKCR.bit.EVBENCLK=0;        // ��� ���������
           SysCtrlRegs.PCLKCR.bit.SCIAENCLK=0;
           SysCtrlRegs.PCLKCR.bit.SCIBENCLK=0;
           SysCtrlRegs.PCLKCR.bit.MCBSPENCLK=0;
           SysCtrlRegs.PCLKCR.bit.SPIENCLK=0;
           SysCtrlRegs.PCLKCR.bit.ECANENCLK=0;
           SysCtrlRegs.PCLKCR.bit.ADCENCLK=0;
           EDIS;
}
